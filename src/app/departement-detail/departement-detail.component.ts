import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router, ParamMap } from '@angular/router';
import { Route } from '@angular/compiler/src/core';

@Component({
  selector: 'app-departement-detail',
  template: `
    <h3>
      Vous avez selectionné avec id= {{departementId}}
    </h3>
    <a (click)="goPrevious()">Precedent</a>
    <a (click)="goNext()">Suivant</a>
    <div>
      <button (click)="gotoDepartement()">Back</button>
    </div>
  `,
  styles: [
  ]
})
export class DepartementDetailComponent implements OnInit {
  public departementId;
  constructor(private route: ActivatedRoute,private router:Router) { }

  ngOnInit(): void {
    //let id = parseInt(this.route.snapshot.paramMap.get('id'));
    //this.departementId =id;
    this.route.paramMap.subscribe((params:ParamMap)=>{
      let id = parseInt(params.get('id'));
      this.departementId=id;
    });
  }
  goPrevious(){
    let previousId =this.departementId -1;
    this.router.navigate(['/departements',previousId]);
  }
  goNext(){
    let nextId =this.departementId+1;
    this.router.navigate(['/departements',nextId]);


  }
  gotoDepartement(){
    let selectedId =  this.departementId ? this.departementId: null;
    this.router.navigate(['departements',{id:selectedId}]);
  }
}
