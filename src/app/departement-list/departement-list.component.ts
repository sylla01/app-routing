import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute,ParamMap} from '@angular/router';
@Component({
  selector: 'app-departement-list',
  template: `
    <h3>
      Departement List!
    </h3>
    <ul class="list-group">
      <li  class="list-group-item" (click)="onSelect(departement)" [class.selected]=" isSelected(departement)" *ngFor="let departement of departments">
      <span class="list-group-item">{{departement.id}}.{{departement.name}}</span>

      </li>
    </ul>
  `,
  styles: [
  ]
})
export class DepartementListComponent implements OnInit {
 public selectedId;
  departments=[
    {"id":1, "name": "Angular"},
    {"id":2, "name": "Ruby"},
    {"id":3, "name": "Npm"},
    {"id":4, "name": "Node"},
    {"id":5, "name": "C++"},
    {"id":6, "name": "JavaScript"},
    {"id":7, "name": "Mongo"}

  ]
  constructor(private router:Router,private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params:ParamMap)=>{
      let id = parseInt(params.get('id'));
      this.selectedId=id;
    });
  }
  onSelect(departement){
    this.router.navigate(['/departements',departement.id]);
 }
 isSelected(departement){
   return departement.id === this.selectedId;
 }
}
